﻿using UnityEngine;
using UnityEngine.UI;

public class BrowserViewer : MonoBehaviour
{
    [SerializeField] private InputField inputLink;
    [SerializeField] private Button buttonOpenLink;
    [SerializeField] private Button buttonBack;

    private void Awake()
    {
        buttonOpenLink.onClick.AddListener(ButtonOpenLinkOnClick);
        buttonBack.onClick.AddListener(ButtonBackOnClick);
    }

    private void ButtonOpenLinkOnClick()
    {
        Application.OpenURL(inputLink.text);
    }

    private void ButtonBackOnClick()
    {
        gameObject.SetActive(false);
    }
}