﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityNative.Toasts.Example;

public class ServiceViewer : MonoBehaviour
{
    [SerializeField] private Button buttonStart;
    [SerializeField] private Button buttonStop;
    [SerializeField] private Button buttonBack;

    private Coroutine _serviceCoroutine;
    private const string LogFileName = "service_log.txt";
    
    private void Awake()
    {
        buttonStart.onClick.AddListener(ButtonStartOnClick);
        buttonStop.onClick.AddListener(ButtonStopOnClick);
        buttonBack.onClick.AddListener(ButtonBackOnClick);
        
        ClearFile();
    }

    private void ButtonBackOnClick()
    {
        gameObject.SetActive(false);
    }

    private void ButtonStopOnClick()
    {
        if (_serviceCoroutine != null)
        {
            StopCoroutine(_serviceCoroutine);
            _serviceCoroutine = null;
        }
    }

    private void ButtonStartOnClick()
    {
        if (_serviceCoroutine == null)
        {
            _serviceCoroutine = StartCoroutine(ServiceCoroutine());
        }
    }

    private IEnumerator ServiceCoroutine()
    {
        var counter = 0;
        while (true)
        {
            yield return new WaitForSeconds(4);
            counter++;
            var msg = $"Current counter is {counter}";
            ShowToast(msg);
            AddToFile(msg);
        }
    }

    protected void ShowToast(string message)
    {
#if UNITY_ANDROID
        UnityNativeToastsHelper.ShowShortText(message);
#endif
    }

    private string GetLogFilePath()
    {
        return Path.Combine(Application.persistentDataPath, LogFileName);
    }

    private void ClearFile()
    {
        File.Create(GetLogFilePath()).Close();
    }

    private void AddToFile(string msg)
    {
        File.AppendAllText(GetLogFilePath(), msg + Environment.NewLine);
    }
}
